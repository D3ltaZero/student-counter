package com.example.deltaprototype;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Integer counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("count", counter);
        super.onSaveInstanceState(outState);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        counter = savedInstanceState.getInt("count");
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void onClickBtnAddStudents(View view) {
        counter++;
        TextView counterView = findViewById(R.id.txt_counter);
        counterView.setText(counter.toString());

        if(counter >= 10000) {
            counterView.setTextSize(30);
        } else if (counter >= 1000) {
            counterView.setTextSize(40);
        }
    }
}